
Include role Postgresql   

- name: postgresql   
  src: https://gitlab.com/library585605/ansible_roles/postgresql.git
  scm: git   
  version: main    

 
Playbook Example      
   
- hosts:    
  gather_facts: true   
  become: true   
  roles:   
    - postgresql

